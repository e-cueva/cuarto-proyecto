package cuartoproyecto;

import java.util.Scanner;

public class Colegio {

    private String nombre;
    private String apellido;
    private String alumnobe;
    private String alumnonobe;
    public int nma;
    Scanner data = new Scanner(System.in);

    public Colegio(String n, String a, String ab, String an, int nm) {
        nombre = n;
        apellido = a;
        alumnobe = ab;
        alumnonobe = an;
        nma = nm; 

    }

    public String getNombre() {
        
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getNma() {
        
        return nma;
    }

    public void setNma(int nma) {
        this.nma = nma;
    }

    public String getAlumnobe() {
        return alumnobe;
    }

    public void setAlumnobe(String alumnobe) {
        this.alumnobe = alumnobe;
    }

    public String getAlumnonobe() {
        return alumnonobe;
    }

    public void setAlumnonobe(String alumnonobe) {
        this.alumnonobe = alumnonobe;
    }

}
