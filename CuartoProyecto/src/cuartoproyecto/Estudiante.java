package cuartoproyecto;

public class Estudiante extends Colegio {

    public int dev;
    public int devan;

    public Estudiante(String n, String a, String ab, String an, int nma) {
        super(n, a, ab, an, nma);
        this.dev = dev;
    }

    public int getDev() {
        int val = nma * 80;
        return val;
    }

    public void setDev(int dev) {
        this.dev = dev;
    }

    public int getDevan() {
        int van = nma * 180;
        return van;
    }

    public void setDevan(int devan) {
        this.devan = devan;
    }

    @Override
    public String toString() {
        String p = (" NOMBRE DEL ESTUDIANTE: "
                + getNombre()
                + "\n APELLIDO DEL ESTUDIANTE : "
                + getApellido()
                + "\n SI POSEE BECA SU MATRICULA SERA DE : $"
                + getDev()
                + "\n SI NO POSEE BECA SU MATRICULA SERA DE : $"
                + getDevan());

        return p;
    }
}
